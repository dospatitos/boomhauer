#!/usr/bin/perl
use strict;
use warnings;
use DB_File;
use JSON::XS;
use Data::Dumper;
$Data::Dumper::Purity = 1;

unlink 'fruit.db' ;
my $db = tie our %hash, 'DB_File', 'fruit.db', O_RDWR|O_CREAT, 0666, $DB_HASH;

# Install DBM Filters
#$db->filter_store_key( sub {$_ = encode_json $_} ); 
$db->filter_store_key( sub {print "store_key> ".Dumper($_)} ); 
#$db->filter_fetch_key( sub {$_ = decode_json $_} );
$db->filter_fetch_key( sub {print "fetch_key> ".Dumper($_)} );

$db->filter_store_value( sub {$_ = encode_json $_} );
$db->filter_fetch_value( sub {$_ = decode_json $_} );

$hash{a}={b=>{c=>123}};
print "---------------\n";
$hash{a}{b}{c}=42; #fails
print "---------------\n";
print ">> $hash{a}{b}{c} \n\n";

$hash{tou}={hou=>9};
print Dumper(\%hash);

undef $db;
untie %hash;

