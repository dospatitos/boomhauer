#!/usr/bin/perl
use strict;
use warnings;
no warnings 'experimental';
use feature qw(switch state);
use List::Util qw(reduce);
use constant { TRUE => 1, FALSE => 0 };
use Term::ANSIColor;
use Regexp::Common;
use DB_File;
use JSON::XS;

use Data::Dumper;
$Data::Dumper::Purity = 1;

# Initial amount of money invested (the number itself is irrelevant, but don't change it!)
use constant INITIAL_AMOUNT => 1000;

# This is 100% - 0.6% (the fee)
use constant MTGOX_FACTOR => 0.994;

sub brain {
	my ($nick, $where, $what) = @_;

	my $db = tie our %storage, 'DB_File', 'players.db', O_RDWR|O_CREAT, 0666, $DB_HASH;
	$db->filter_store_value( sub {$_ = encode_json $_} );
	$db->filter_fetch_value( sub {$_ = decode_json $_} );

	# creo este ref por limitaciones de tie de HoH en perl
	my $tmp_storage = $storage{$nick};

	given($what){
		### Commands ###
		when (/^\$buy$/)		{ buy(\$tmp_storage, $nick); }
		when (/^\$sell$/)		{ sell(\$tmp_storage, $nick); }
		when (/^\$status$/)		{ send_msg($where, status(\$storage{$nick}, $nick, FALSE)); }
		when (/^\$status (.*)/)	{  $1 =~ /(\A[a-z_\-\[\]\\^{}|`][a-z0-9_\-\[\]\\^{}|`]{2,15}\z)/i ? send_msg($where, status(\$storage{$1}, $1, TRUE)) : send_msg($where, "'$1' doesn't look like a valid nickname man."); }
		when (/^\$top$/)		{ send_msg($where, show_top()); }
		when (/^\$players$/)	{ send_msg($where, "There's ".(scalar keys %storage)." dang'ol players man"); }
		when (/^\$breakeven/)	{ send_msg($where, 'Yo man, I think your break even point is $'.sprintf('%.2f',breakeven($tmp_storage->{operations}%2, $tmp_storage->{last_price}))); }
		when (/^\$price/)		{ send_msg($nick, "MtGox last price: ".lastPrice()); }
		when (/^\$public/)		{ turn_public(\$tmp_storage, TRUE); }
		when (/^\$private/)		{ turn_public(\$tmp_storage, FALSE); }
		when (/^\$help/)		{ send_msg($where, "hey $nick, commands are: \$buy, \$sell, \$status [nickname], \$top, \$players, \$breakeven, \$price, \$public, \$private"); }

		### Debugging and ugly hacky stuff :3 ###
		when (/^\$debug/)		{ print colored(('-'x(80).$/.Dumper(\%storage).$/), 'bold blue'); }
		#when (/^\$create_top/) { for my $tmp_nick (keys %storage){my $user_data = $storage{$tmp_nick}; top(\%top, roi(INITIAL_AMOUNT, $user_data->{investment}), $tmp_nick);} }
		#when (/^\$addnewkey/)	{ for my $player (keys %storage){my $tmp=$storage{$player}; if(not exists $tmp->{last_price}){print(GREEN,"Adding new key to $player... ",RESET); $tmp->{last_price}=-1; $storage{$player}=$tmp; print "OK\n"; } } }
		#when (/^\$checkvalue/)	{ for my $player (keys %storage){my $tmp=$storage{$player}; if($tmp->{last_price} < 0){$tmp->{operations}==1?print(YELLOW,"$player\n",RESET):print(RED,"$player\n",RESET); } } }
		#when (/^\$changevalue/)	{ for my $player (keys %storage){my $tmp=$storage{$player}; if($tmp->{last_price} < 0){if($tmp->{operations}==1){$tmp->{last_price}=$tmp->{initial_price};}else{$tmp->{last_price}=42;}; $storage{$player}=$tmp; } } }

		### Easter eggs ###
		when (/^\$hi/)			{ send_msg($where, rand_elem(@{["Yo $nick!", 'Yo man.']})); }
		when (/^\$boomhauer/)	{ send_msg($where, 'whut?'); }

		### Undefined command reply ###
		# FIX commented because "I bought at $10 two years ago!" or "Making $$$ !!!"
		#when (/^\$.*/) {print "\nHOHO!\n\n";} # do nothing
		default {
#			send_msg($where, rand_elem(@{['huh?', 'I don\'t know what that means man...']}));
		}
	}

	# check if $tmp_storage is not undef and also if we did something that
	# could change it's content (ie buy or sell commands)
	if(defined $tmp_storage && $what=~/\$(buy|sell|public|private)/) {
		print colored("saving...\n", 'yellow');
		$storage{$nick} = $tmp_storage ;
	}
	undef $db;
	untie %storage;
}

sub rand_elem {
	return $_[rand @_];
}

sub buy {
	my ($userdata, $nick) = @_;
	my $current_price = lastPrice();
	
	if(not defined $$userdata) {
		# This is a new player :D
		print colored(">> NEW PLAYER: $nick <<\n", 'yellow');
		send_msg($nick, "Welcome new player! Please try \$help refer to this thread for more info: https://bitcointalk.org/index.php?topic=398023.0");
		$$userdata = {
							initial_date => time,
							initial_price => $current_price,
							investment => INITIAL_AMOUNT,
							bitcoins => 0,
							roi_last => 0,
							operations => 0,
							registration_status => undef,
							address => undef,
							public => TRUE,
							last_price => -1,
		};
	}
	if($$userdata->{bitcoins}) {
		# esta intentando hacer mas de un buy seguido
		send_msg($nick, "You already bought man...");
	} else {
		$$userdata->{operations}++;
		$$userdata->{bitcoins} = ($$userdata->{investment} / $current_price) * MTGOX_FACTOR;
		$$userdata->{last_price} = $current_price;

		send_msg($nick, "bought @ $current_price");
	}
}

sub sell {
	my ($userdata, $nick) = @_;
	my $current_price = lastPrice();

	if(not defined $$userdata) {
		send_msg($nick, "Yo man, it's like I always say man: you gotta spend money to make money. Try dang ol \$help if you need help...");

		return;
	}

	if($$userdata->{bitcoins}) {
		my $C1 = $$userdata->{bitcoins} * $current_price * MTGOX_FACTOR;
		$$userdata->{operations}++;
		$$userdata->{roi_last} = roi($$userdata->{investment}, $C1);
		$$userdata->{investment} = $C1;
		$$userdata->{bitcoins} = 0;
		$$userdata->{last_price} = $current_price;

		send_msg($nick, "sold @ $current_price");

		tie our %top, 'DB_File', 'top.db';
		top(\%top, roi(INITIAL_AMOUNT, $$userdata->{investment}), $nick);
		untie %top;
	} else {
		# esta intentando hacer mas de un sell seguido
		send_msg($nick, "You already sold man...");
	}
}

sub top {
	my ($top, $total_roi, $nick) = @_;

	$top->{$nick} = $total_roi;
	if(keys %$top == 6) {
		delete $top->{reduce {$top->{$a} < $top->{$b} ? $a : $b} keys %$top};
	} else {
		if(keys %$top > 6) {
			print colored('-'x(80)."\nIMPOSSIBRU ERROR! The top hash has more than 6 items >:(\n\n", 'bold red');
		}
	}
}

sub show_top {
	tie our %top, 'DB_File', 'top.db';
	
	my $result = "Top Five™ - ".join ' | ', map {sprintf("$_: %.2f\%",$top{$_})} sort {$top{$b} <=> $top{$a}} keys %top;

	untie %top;
	return $result;
}

sub status {
	my ($userdata, $nick, $check_public) = @_;
	my $output;

	if(defined $$userdata) {
		if($$userdata->{public} == TRUE || $check_public == FALSE) {
			my $total_roi = sprintf '%.2f',roi(INITIAL_AMOUNT, $$userdata->{investment});
			my $last_roi = sprintf '%.2f',$$userdata->{roi_last};
			my $last_trade = 'dunno';
			if(defined $$userdata->{last_price}){
				$last_trade = sprintf+(($$userdata->{operations}%2?'bought':'sold').' @ %.2f'),$$userdata->{last_price};
			}
			my $holders_roi = sprintf '%.2f',roi(INITIAL_AMOUNT, (INITIAL_AMOUNT/$$userdata->{initial_price})*lastPrice());
			my $time_playing = int((time - $$userdata->{initial_date}) / (60*60*24));
			$time_playing = "$time_playing day".($time_playing==1?'':'s');
			$output = "\[$nick\] total ROI: $total_roi\% | last ROI: $last_roi\% | #trades: $$userdata->{operations} | last trade: $last_trade | time playing: $time_playing | holders ROI: $holders_roi%";
		} else {
			$output = "Sorry man, I can't show you any dang ol info bout $nick man, it's private...";
		}
	} else {
		$output = "Sorry man, I can't find any dang ol info bout $nick.";
	}

	return $output;
}

sub turn_public {
	my ($userdata, $public) = @_;

	if(defined $$userdata) {
		$$userdata->{public} = $public;
	}
}

sub roi {
	my ($C0, $C1) = @_;
	return (($C1-$C0) / $C0) * 100;
}

sub breakeven {
	# if $type == 0 sold, break even down. Otherwise bought, normal break even
	my ($type, $last_price) = @_;

	if($type == 1) {
		return ($last_price / (MTGOX_FACTOR ** 2));
	} else {
		return ($last_price * (MTGOX_FACTOR ** 2));
	}
}

# cuidado puede retornar 0 y undef...
sub lastPrice {
	state $last_time = 0;
	state $last_price = 0;

	# esperamos 11 segundos para que gox no nos haga ban
	if(time() > $last_time+11){
		$last_time = time();
		my $json = get('http://data.mtgox.com/api/2/BTCUSD/money/ticker');

		# some debug info:
		print colored("MtGox JSON: $json\n", 'bright_black');

		if(defined $json && $json=~/"last":{"value":"($RE{num}{real})"/){
			$last_price = $1;
			return $last_price;
		}
	} else {
		return $last_price;
	}

	return;
}

