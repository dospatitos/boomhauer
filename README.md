boomhauer
=========

Dang ol' IRC bot man

run (full output logging):

	nice -10 perl boomhauer.pl channels 2>&1 | tee -a boomhauer.full.log

you may like to add the following code to your crontab:

	@reboot screen -d -m /usr/bin/perl <full-path-for-boomhauer.pl> <channels>
	@daily tar -zcf <full-path-to-your-backup-dir>`date +\%s`.tgz -C <full-path-to-boomhauers-dir> players.db

Critical errors are already log'd in *boomhauer.log*

check out your system time, keep it syncronized with ntp, otherwise dummy.pl could be not reloaded after changes...

