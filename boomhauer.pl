#!/usr/bin/perl
use strict;
use warnings;
no warnings 'experimental';
use feature qw(switch state);
use Term::ANSIColor;
use LWP::Simple;
use POE;
use POE::Component::IRC;
use POE::Component::IRC::State;
use POE::Component::IRC::Common qw( :ALL );
use POE::Component::IRC::Plugin::Connector;
use POE::Component::IRC::Plugin::AutoJoin;
use XML::Simple;
use Log::Log4perl;

# autoflush
$|=1;

my $version = '0.3';

$0 =~ s/[^\/]*$//;
chdir $0 if $0;

die "Unspecified channel" if !@ARGV;
my @channels = map {'#'.$_} @ARGV;

my %logconf = (
	'log4perl.rootLogger' => 'DEBUG, Logfile',

	'log4perl.appender.Logfile' => 'Log::Log4perl::Appender::File',
	'log4perl.appender.Logfile.filename' => 'boomhauer.log',
	'log4perl.appender.Logfile.mode' => 'append',
	'log4perl.appender.Logfile.layout' => 'Log::Log4perl::Layout::PatternLayout',
	'log4perl.appender.Logfile.layout.ConversionPattern' => '%d: [%r] %m%n',
);
Log::Log4perl->init(\%logconf);
my $log = Log::Log4perl->get_logger();

my $config = XMLin('config.xml');

my $nickname = $config->{nick};
my $username = $config->{user};
my $server = $config->{server}->{content};
my $usessl = $config->{server}->{ssl} eq "yes" ? 1 : 0;
my $port = $config->{port};
# Si el servidor dispone de NickServ este es el password
my $nickpass = $config->{nickpass};

my $ircname = 'Boomhauer v'.$version;

my $irc = POE::Component::IRC->spawn( 
	nick => $nickname,
	username => $username,
	ircname => $ircname,
	server => $server,
	port => $port,
	usessl => $usessl,
) or die "Goodbye cruel world! $!";

POE::Session->create(
	package_states => [
		main => [ qw(_default _start exit_handle irc_001 irc_public irc_msg irc_raw_out irc_join irc_part irc_quit irc_kick irc_nick) ],
	]
);

$log->info('-- initiating --');
$poe_kernel->run();

sub exit_handle {
	print colored("\n$$ SIGINT\n",'bold red');
	$log->info('-- shutting down --');
	#$_[KERNEL]->sig_handled(); # XXX not working?
}

sub _start {
	my $heap = $_[HEAP];

	$_[KERNEL]->sig( INT => 'exit_handle' );
	$_[KERNEL]->sig( QUIT => 'exit_handle' );

	$irc->yield( register => 'all' );

	$heap->{connector} = POE::Component::IRC::Plugin::Connector->new();
	$irc->plugin_add( 'Connector' => $heap->{connector} );

	$irc->yield( connect => { } );

	$irc->plugin_add( 'AutoJoin', POE::Component::IRC::Plugin::AutoJoin->new(Channels=>\@channels) );

	return;
}

sub irc_001 {
	my $sender = $_[SENDER];
	my $irc = $sender->get_heap();

	print "Connected to ", $irc->server_name(), "\n";

	$irc->yield( nickserv => 'IDENTIFY' => $nickpass );
	return;
}

sub irc_public {
	my ($sender, $who, $where, $what) = @_[SENDER, ARG0 .. ARG2];
	my $nick = ( split /!/, $who )[0];
	my $channel = $where->[0];

	print colored('irc_public: ', 'bold green'), "<$nick> $what\n";

	evaluate($nick, $channel, $what);

	return;
}

sub irc_msg {
	my ($who, $recipients, $what) = @_[ARG0 .. ARG2];
	my $nick = ( split /!/, $who )[0];

	print colored('irc_msg: ', 'bold green'), "<$nick> $what\n";

	evaluate($nick, $nick, $what);

	return;
}

sub irc_raw_out {
	my $what = $_[ARG0];

	print colored('irc_raw_out: ', 'bold green'), "$what\n";

	return;
}

sub irc_join {
	my ($who, $where) = @_[ARG0, ARG1];
	my $nick = ( split /!/, $who )[0];
	my $channel = $where;

	print "irc_join: $nick\n";

	return;
}

sub irc_part {
	my ($who, $where) = @_[ARG0, ARG1];
	my $nick = ( split /!/, $who )[0];
	my $channel = $where;

	print "irc_part: $nick\n";

	return;
}

sub irc_quit {
	my ($who, $where) = @_[ARG0, ARG1];
	my $nick = ( split /!/, $who )[0];
	my $channel = $where;

	print "irc_quit: $nick\n";

	return;
}

sub irc_kick {
	my ($kicker, $where, $kickee) = @_[ARG0, ARG1, ARG2];

	print "irc_kick: $kicker kicked $kickee\n";

	return;
}

sub irc_nick {
	my ($who, $newnick) = @_[ARG0, ARG1];
	my $oldnick = ( split /!/, $who )[0];

	print "irc_nick: $oldnick changed to $newnick\n";

	return;
}

sub _default {
	my ($event, $args) = @_[ARG0 .. $#_];
	my @output = ( "$event:" );

	for my $arg (@$args) {
		if(defined $arg) {
			if(ref $arg eq 'ARRAY'){
				push( @output, '[' . join(', ', @$arg ) . ']' );
			} else {
				push ( @output, "'$arg'" );
			}
		}
	}
	print colored(join(' ', @output), 'bright_black'), "\n";
	return 0;
}

# call brain function
sub evaluate {
	my ($nick, $channel, $what) = @_;

	if($what=~m/^\$\w/){
		print colored("evaluating '$what'\n", 'bold bright_white');
		&evaluator;
		eval 'brain($nick, $channel, $what)';
		if ($@) {
			send_me($channel, "is dying D:");
			print colored('-'x(80)."\nCRITICAL ERROR\n$@\n", 'bold red');
			$log->error("CRITICAL ERROR EVALUATING '$what' (<$nick>):\n".$@);
		}
	}
}

# read and check dummy file
sub evaluator {
	my $dummy;
	state $last_mtime = 0;

	my $mtime = (stat("dummy.pl"))[9];
	if($mtime > $last_mtime){
		$last_mtime = $mtime;
		open my $FD, "dummy.pl" or die $!;
		while(<$FD>){
			$dummy .= $_;
		}
		close $FD;

		eval $dummy;
		if ($@) {
			print $@;
		}
	}
}

sub send_msg {
	$irc->yield( privmsg => $_[0] => $_[1] );
}

sub send_me {
	$irc->yield( ctcp => $_[0] => 'ACTION '.$_[1])
}

sub send_notice {
	$irc->yield( notice => $_[0] => $_[1] );
}

sub set_mode {
	$irc->yield( mode => $_[0] => $_[1] );
}

sub delay_event {
	$irc->delay( $_[0], $_[1] );
}

